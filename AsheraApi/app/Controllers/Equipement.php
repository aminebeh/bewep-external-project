<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Equipement extends Connexion
{
    use ResponseTrait;
    
    protected $dataGso;

	public function get($idPartieOuvrage){ // id le l'entité de gestion, pour test id = 143
		$query = $this->db->query(
            "SELECT  IdEquipement, NomEquipement
            FROM gso_beweb.equipement
            WHERE IdPartieOuvrage = $idPartieOuvrage"
        );
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
	}
}
