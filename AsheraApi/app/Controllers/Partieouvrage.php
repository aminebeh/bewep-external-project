<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Partieouvrage extends Connexion
{
    use ResponseTrait;
    
    protected $dataGso;

	public function get($idOuvrage){ // id le l'ouvrage gestion, pour test id = 2925
		$query = $this->db->query(
            "SELECT  IdPartieOuvrage, NomPartieOuvrage
            FROM gso_beweb.partieouvrage
            WHERE IdOuvrage = $idOuvrage"
        );
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
	}
}
