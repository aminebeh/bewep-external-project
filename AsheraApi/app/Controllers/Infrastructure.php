<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Infrastructure extends Connexion
{
    use ResponseTrait;
    
    protected $dataGso;

	public function get($idEntiteGestion){ // id le l'entité de gestion, pour test id = 143
		$query = $this->db->query(
            "SELECT NomInfrastructure , IdInfrastructure
            FROM gso_beweb.infrastructures
            WHERE IdEntite_Gestion = $idEntiteGestion"
        );
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
    }
    
}
