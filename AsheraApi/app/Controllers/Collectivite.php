<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Collectivite extends Connexion
{
    use ResponseTrait;
    
    protected $dataGso;

	public function get($idClient){
		$query = $this->db->query("SELECT A.ID_Collectivite , B.nom_organisme
            FROM gso_beweb.collectivite AS A 
            LEFT JOIN gso_beweb.organisme AS B ON (A.ID_Organisme = B.ID_Organisme)
            WHERE A.ID_Client =" . $idClient);
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
	}
}
