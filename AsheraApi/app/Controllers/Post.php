<?php

namespace App\Controllers;
use CodeIgniter\Database\Query;

class Post extends BaseController
{

	protected $db;

	public function __construct()
	{
		$this->db = \Config\Database::connect();
	}

	public function index()
	{
		return view('welcome_message');
	}

	public function token($val)
	{
		$data = array(
			'token'=>$val
 		);

		$this->db->table('token')->insert($data);

	}


}
