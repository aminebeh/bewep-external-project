<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Ouvrage extends Connexion
{
    use ResponseTrait;

    protected $dataGso;
    protected $dataVisite;

	public function get($idInfrastructure){ // id le l'entité de gestion, pour test id = 143
		$query = $this->db->query(
            "SELECT  NomOuvrage, IdOuvrage
            FROM gso_beweb.ouvrages
            WHERE IdInfrastructure = $idInfrastructure"
        );
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
    }
    
    public function post() {

        $dataVisite = ['id_ouvrage' => '256', 'id_client' => '2', 'nom_ouvrage' => 'test', 'id_visite_status' => '2', 'commentaire' => 'ca marche'];
        $this->db->table('visite')->insert($dataVisite);

        /* $query = $this->db->query("SELECT * FROM visite");
        $this->data = json_encode($query->getResult());
        return $this->data; */
    }
}
