<?php 

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class EntiteGestion extends Connexion
{
    use ResponseTrait;
    
    protected $dataGso;

	public function get($idCollectivite){
		$query = $this->db->query(
            "SELECT NomUniteGestion , IdEntite_Gestion
            FROM gso_beweb.entite_gestion
            WHERE ID_Collectivite = $idCollectivite");
        $this->dataGso = $query->getResult();
        return $this->response->setJSON($this->dataGso);
	}
}
