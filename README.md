# mtp01_ashera

**PROJECT DETAILS**



This project is about a mobile application to help a company in differents audits in the sector 
of the water sanitation. It's working with and without internet, the worker is loading in advance his differents
future locations and he can leave some comments, pictures about material in differents infrastructure.
At the end of the day or when he got the time, he will synchronyse his work from his phone to the database.
See the following [link](https://gitlab.com/fabrique-beweb/mtp01_ashera/blob/conception/Ressources/Pr%C3%A9sentation%20Projet/Visite_site_BeWeb.pptx) for more details

**RESSOURCES**

[Trello](https://trello.com/b/xVNRNwJ7/ashera-project)


[IONIC Docs](https://ionicframework.com/docs)


["Conception" Branch ](https://gitlab.com/fabrique-beweb/mtp01_ashera/tree/conception/)


**INSTALLATION**

[DataBase SQL](https://gitlab.com/fabrique-beweb/mtp01_ashera/blob/conception/Ressources/DataBase/gso_beweb.sql)


[NodeJS Installation](https://nodejs.org/en/)


[Android Studio Installation Instructions](https://gitlab.com/fabrique-beweb/mtp01_ashera/tree/conception/Ressources/Install%20Android%20Sutdio)


Install Ionic:
`npm install -g ionic cordova`

Launch server:
`ionic serve`

Stop server:
Press CTR + C

*[Android Migration](https://ionicframework.com/docs/building/ios):*


on IDE=>
`ionic cordova build android`

on Android Studio=>
`ionic cordova prepare android`
...

*[IOS Migration](https://ionicframework.com/docs/building/android):*
